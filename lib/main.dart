import 'package:flutter/material.dart';
import 'package:pokemon_app/pages/page_colosseum_ing.dart';
import 'package:pokemon_app/pages/page_colosseum_stage.dart';
import 'package:pokemon_app/pages/page_pokemon_list.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const PagePokemonList(),
    );
  }
}