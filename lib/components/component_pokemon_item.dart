import 'package:flutter/material.dart';
import 'package:pokemon_app/model/pokemon_item.dart';

class ComponentPokemonItem extends StatelessWidget {
  const ComponentPokemonItem({
    super.key,
    required this.pokemonItem,
    required this.callback,

  });

  final PokemonItem pokemonItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Image.asset(
              'assets/${pokemonItem.pokeImg}',
              width: 120,
              height: 120,
              fit: BoxFit.contain,
            ),
            Text('도감 번호 : ${pokemonItem.id}'),
            Text('이름 : ${pokemonItem.pokeName}'),
            Text('속성 : ${pokemonItem.pokeType}'),
            Text('체력 : ${pokemonItem.hp}'),
            Text('공격력 : ${pokemonItem.attack}'),
            Text('방어력 : ${pokemonItem.defense}'),
            Text('스피드 : ${pokemonItem.speed}'),
            Text('등급 : ${pokemonItem.pokeRank}'),
          ],
        ),
      ),
    );
  }
}
