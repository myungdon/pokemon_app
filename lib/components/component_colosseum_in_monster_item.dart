import 'package:flutter/material.dart';
import 'package:pokemon_app/model/colosseum_item.dart';
import 'package:pokemon_app/model/pokemon_item.dart';

class ComponentColosseumInMonsterItem extends StatefulWidget {
  const ComponentColosseumInMonsterItem({
    super.key,
    required this.colosseumItem,
    required this.callback,
    required this.isMyTurn,
    required this.isLive,
    required this.currentHp,
  });
  
  final ColosseumItem colosseumItem;
  final VoidCallback callback;
  final bool isMyTurn;
  final bool isLive;
  final num currentHp;

  @override
  State<ComponentColosseumInMonsterItem> createState() => _ComponentColosseumInMonsterItemState();
}

class _ComponentColosseumInMonsterItemState extends State<ComponentColosseumInMonsterItem> {
  double _calculateHpPercent() {
    return widget.currentHp / widget.colosseumItem.hp; //체력 계산
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width / 3,
            height: MediaQuery.of(context).size.width / 3,
            child: Image.asset(
              widget.isLive ? widget.colosseumItem.pokeImg : 'assets/lose.jpg', // 살아 있으면 포켓몬 사진, 죽었으면 패배 사진
              fit: BoxFit.fill,
            ),
          ),
          Text(widget.colosseumItem.pokeName),
          Text('현재 체력 ${widget.currentHp}'),
          Text('스피드 ${widget.colosseumItem.speed}'),
          Text('공격력 ${widget.colosseumItem.attack}'),
          Text('방어력 ${widget.colosseumItem.defense}'),
          SizedBox(
            width: MediaQuery.of(context).size.width / 2,
            height: MediaQuery.of(context).size.width / 50,
            child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            child: LinearProgressIndicator(  // 체력바 생성
              value: _calculateHpPercent(),
              valueColor: _calculateHpPercent() < 0.3 ? AlwaysStoppedAnimation<Color>(Colors.redAccent) : AlwaysStoppedAnimation<Color>(Colors.lightGreenAccent),
            ),
            ),
          ),
          (widget.isMyTurn && widget.isLive) ? // 턴이고 살아있으면 공격 버튼 생성
          Container(
            margin: EdgeInsets.all(20),
            child: OutlinedButton(
              onPressed: widget.callback,
              child: const Text('공격'),
            ),
          ) : Container(),    // 하나라도 안 맞으면 빈 칸
        ],
      ),
    );
  }
}
