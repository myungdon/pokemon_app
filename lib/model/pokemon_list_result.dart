import 'package:pokemon_app/model/pokemon_item.dart';

class PokemonListResult{
  String msg;
  num code;
  List<PokemonItem> list;
  num totalCount;

  PokemonListResult(this.msg, this.code, this.list, this.totalCount);

  factory PokemonListResult.fromJson(Map<String, dynamic> json) {
    return PokemonListResult(
        json['msg'],
        json['code'],
        json['list'] != null ?
          (json['list'] as List).map((e) => PokemonItem.fromJson(e)).toList()
            : [],
        json['totalCount'],
    );
  }
}