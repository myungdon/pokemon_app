import 'package:pokemon_app/model/pokemon_response.dart';

class PokemonDetailResult {
  String msg;
  num code;
  PokemonResponse data;

  PokemonDetailResult(this.msg, this.code, this.data);

  factory PokemonDetailResult.fromJson(Map<String, dynamic> json){
    return PokemonDetailResult(
        json['msg'],
        json['code'],
        PokemonResponse.fromJson(json['data']),
    );
  }
}