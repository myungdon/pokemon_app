import 'package:pokemon_app/model/colosseum_in_monsters.dart';

class ColosseumStateResult {
  ColosseumInMonsters data;

  ColosseumStateResult(this.data);

  factory ColosseumStateResult.fromJson(Map<String, dynamic>json) {
    return ColosseumStateResult(
        ColosseumInMonsters.fromJson(json['data'])
    );
  }
}