import 'package:pokemon_app/model/colosseum_item.dart';

class ColosseumInMonsters {
  ColosseumItem monster1;
  ColosseumItem monster2;

  ColosseumInMonsters(this.monster1, this.monster2);

  factory ColosseumInMonsters.fromJson(Map<String, dynamic>json) {
    return ColosseumInMonsters(
      ColosseumItem.fromJson(json['monster1']),
      ColosseumItem.fromJson(json['monster2']),
    );
  }
}