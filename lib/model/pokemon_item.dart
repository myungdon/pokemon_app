class PokemonItem {
  String pokeImg;
  num id;
  String pokeName;
  String pokeType;
  num hp;
  num attack;
  num defense;
  num speed;
  String pokeRank;

  PokemonItem(this.pokeImg, this.id, this.pokeName, this.pokeType, this.hp, this.attack, this.defense, this.speed, this.pokeRank);

  factory PokemonItem.fromJson(Map<String, dynamic> json) {
    return PokemonItem(
      json['pokeImg'],
      json['id'],
      json['pokeName'],
      json['pokeType'],
      json['hp'],
      json['attack'],
      json['defense'],
      json['speed'],
      json['pokeRank'],
    );
  }
}
