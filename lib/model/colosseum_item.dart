class ColosseumItem {
  num colosseumId;
  num monsterId;
  String pokeImg;
  String pokeName;
  num hp;
  num attack;
  num defense;
  num speed;

  ColosseumItem(this.colosseumId, this.monsterId, this.pokeImg, this.pokeName, this.hp, this.attack, this.defense, this.speed);

  factory ColosseumItem.fromJson(Map<String, dynamic> json) {
    return ColosseumItem(
      json['colosseumId'],
      json['monsterId'],
      json['pokeImg'],
      json['pokeName'],
      json['hp'],
      json['attack'],
      json['defense'],
      json['speed'],
     );
  }
}
