class CommonResult {
  String msg;
  num code;

  CommonResult(this.msg, this.code);

  factory CommonResult.fromJson(Map<String, dynamic>json) {
    return CommonResult(
      json['msg'],
      json['code'],
    );
  }
}