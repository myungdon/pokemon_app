import 'package:flutter/material.dart';
import 'package:pokemon_app/components/component_pokemon_item.dart';
import 'package:pokemon_app/model/pokemon_item.dart';
import 'package:pokemon_app/pages/page_colosseum_stage.dart';
import 'package:pokemon_app/pages/page_pokemon_detail.dart';
import 'package:pokemon_app/repository/repo_pokemon.dart';

class PagePokemonList extends StatefulWidget {
  const PagePokemonList({super.key});

  @override
  State<PagePokemonList> createState() => _PagePokemonListState();
}

class _PagePokemonListState extends State<PagePokemonList> {
  List<PokemonItem> _list = [];

  Future<void> _loadList() async {
    await RepoPokemon().getPokemons()
        .then((res) => {
      setState(() {
        _list = res.list;
      })
    })
        .catchError((err) => {
      debugPrint(err)
    });
  }

  @override
  void initState() {
    super.initState();
    _loadList();
  }

  @override
  Widget build(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          'assets/pokemon_logo.png',
          width: phoneWidth / 4,
          height: phoneWidth / 20,
        ),
        backgroundColor: Colors.cyanAccent,
        centerTitle: true,
      ),
      drawer: Drawer(
          child: ListView(
              padding: EdgeInsets.zero,
              children: [
                const UserAccountsDrawerHeader(
                  currentAccountPicture: CircleAvatar(
                    backgroundImage: AssetImage('assets/bunny.gif'),
                  ),
                  otherAccountsPictures: [
                    CircleAvatar(
                      backgroundImage: AssetImage('assets/profile.png'),
                    )
                  ],
                  accountEmail: Text('dev.yakkuza@gmail.com'),
                  accountName: Text('Dev Yakuza'),
                  decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(40),
                        bottomRight: Radius.circular(40),
                      )),
                ),
                ListTile(
                    title: const Text('Item 1'),
                    onTap: () => Navigator.pop(context)
                ),
                ListTile(
                    title: const Text('Item 2'),
                    onTap: () => Navigator.pop(context)
                )
              ]
          )
      ),
      body: Container(
        margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
        child: GridView.builder(
            shrinkWrap: true,
            itemCount: _list.length,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              childAspectRatio: 3 / 4,
              mainAxisSpacing: 5,
              crossAxisSpacing: 5,
            ),
            itemBuilder: (BuildContext context, int idx) {
              return ComponentPokemonItem(pokemonItem: _list[idx], callback: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageColosseumStage(id: _list[idx].id)));
              });
            }
        ),
      ),
    );
  }
}
