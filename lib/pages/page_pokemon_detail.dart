// import 'package:flutter/material.dart';
// import 'package:pokemon_app/model/pokemon_item.dart';
// import 'package:pokemon_app/model/pokemon_response.dart';
// import 'package:pokemon_app/pages/page_colosseum_ing.dart';
// import 'package:pokemon_app/repository/repo_colosseum.dart';
// import 'package:pokemon_app/repository/repo_pokemon.dart';
//
// class PagePokemonDetail extends StatefulWidget {
//   const PagePokemonDetail({
//     super.key,
//     required this.id
//   });
//
//   final num id;
//
//   @override
//   State<PagePokemonDetail> createState() => _PagePokemonDetailState();
// }
//
// class _PagePokemonDetailState extends State<PagePokemonDetail> {
//   PokemonItem? monster1; // 없을 수도 있어
//   PokemonItem? monster2;
//   PokemonResponse? _detail;
//
//   // 현재 결투장에 진입중인 몬스터들 정보 가져오기
//   Future<void> _loadDetail() async {
//     await RepoColosseum().getCurrentState()
//         .then((res) => {
//       setState(() {
//         monster1 = res.data.monster1;
//         monster2 = res.data.monster2;
//       })
//     });
//   }
//
//   // 결투장에서 몬스터 퇴출시키기
//   Future<void> _delMonster(num colosseumId) async {
//     await RepoColosseum().delColosseumOut(colosseumId)
//         .then((res) => {
//       _loadDetail() // 퇴출에 성공하면 현재 결투장에 진입중인 몬스터 정보 새로 가져와서 갱신
//     });
//   }
//
//   @override
//   void initState() {
//     super.initState();
//     _loadDetail();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     double phoneWidth = MediaQuery.of(context).size.width;
//
//     return Scaffold(
//       appBar: AppBar(
//         title: Image.asset(
//           'assets/pokemon_logo.png',
//           width: phoneWidth / 4,
//           height: phoneWidth / 20,
//         ),
//         backgroundColor: Colors.cyanAccent,
//         centerTitle: true,
//       ),
//       body: _buildBody(context),
//     );
//   }
//
//   Widget _buildBody(BuildContext context) {
//     return SingleChildScrollView(
//       child: Column(
//         children: [
//           monster1 != null ?
//           Container(
//             child: Column(
//               children: [
//                 Text(monster1!.pokeName),
//                 OutlinedButton(
//                     onPressed: () {
//                       _delMonster(monster1!.colosseumId);
//                     },
//                     child: Text('퇴장')
//                 )
//               ],
//             ),
//           ) : Container(),
//
//           monster2 != null ?
//           Container(
//             child: Column(
//               children: [
//                 Text(monster2!.pokeName),
//                 OutlinedButton(
//                     onPressed: () {
//                       _delMonster(monster2!.colosseumId);
//                     },
//                     child: Text('퇴장')
//                 )
//               ],
//             ),
//           ) : Container(),
//
//           monster1 != null && monster2 != null ?
//           OutlinedButton(
//             onPressed: () {
//               Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageColosseumIng(monster1: monster1!, monster2: monster2!)));
//             },
//             child: Text('배틀'),
//           ) : Container(),
//         ],
//       ),
//     );
//   }
//   //   if (_detail == null) {
//   //     return Text('데이터 로딩중');
//   //   } else {
//   //     return Center(
//   //       child: Column(
//           children: [
//             Image.asset("assets/${_detail!.pokeImg}"),
//             Text('도감 번호 : ${widget.id}'),
//             Text('이름 : ${_detail!.pokeName}'),
//             Text('속성 : ${_detail!.pokeType}'),
//             Text('체력 : ${_detail!.hp}'),
//             Text('공격력 : ${_detail!.attack}'),
//             Text('방어력 : ${_detail!.defense}'),
//             Text('스피드 : ${_detail!.speed}'),
//             Text('등급 : ${_detail!.pokeRank}'),
//             Text('소개 : ${_detail!.introduction}'),
//           ],
//   //       ),
//   //     );
//   //   }
//   // }
// }
