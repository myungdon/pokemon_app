import 'package:flutter/material.dart';
import 'package:pokemon_app/model/colosseum_item.dart';
import 'package:pokemon_app/model/pokemon_item.dart';
import 'package:pokemon_app/pages/page_colosseum_ing.dart';
import 'package:pokemon_app/repository/repo_colosseum.dart';

class PageColosseumStage extends StatefulWidget {
  const PageColosseumStage({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<PageColosseumStage> createState() => _PageColosseumStageState();
}

class _PageColosseumStageState extends State<PageColosseumStage> {
  ColosseumItem? monster1; // 없을 수도 있어
  ColosseumItem? monster2;

  // 현재 결투장에 진입중인 몬스터들 정보 가져오기
  Future<void> _loadDetail() async {
    await RepoColosseum().getCurrentState()
        .then((res) => {
          setState(() {
            monster1 = res.data.monster1;
            monster2 = res.data.monster2;
          })
    });
  }

  // 결투장에서 몬스터 퇴출시키기
  Future<void> _delMonster(num colosseumId) async {
    await RepoColosseum().delColosseumOut(colosseumId)
        .then((res) => {
      _loadDetail() // 퇴출에 성공하면 현재 결투장에 진입중인 몬스터 정보 새로 가져와서 갱신
    });
  }

  // 페이지 생성되고 화면에 부착하기전에 데이터부터 준다
  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          'assets/pokemon_logo.png',
          width: phoneWidth / 4,
          height: phoneWidth / 20,
        ),
        backgroundColor: Colors.cyanAccent,
        centerTitle: true,
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            Row(
              children: [
                monster1 != null ?
                Container(
                  width: phoneWidth / 2,
                  child: Column(
                    children: [
                      Image.asset(monster1!.pokeImg),
                      Text(monster1!.pokeName),
                      OutlinedButton(
                          onPressed: () {
                            _delMonster(monster1!.colosseumId);
                          },
                          child: Text('퇴장')
                      )
                    ],
                  ),
                ) : Container(),

                monster2 != null ?
                Container(
                  width: phoneWidth / 2,
                  child: Column(
                    children: [
                      Image.asset(monster2!.pokeImg),
                      Text(monster2!.pokeName),
                      OutlinedButton(
                          onPressed: () {
                            _delMonster(monster2!.colosseumId);
                          },
                          child: Text('퇴장')
                      )
                    ],
                  ),
                ) : Container(),
              ],
            ),
            const SizedBox(width: 30),
            SizedBox(
              child:
                monster1 != null && monster2 != null ?
                OutlinedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageColosseumIng(monster1: monster1!, monster2: monster2!)));
                  },
                  child: Text('배틀'),
                ) : Container(),
            )
          ],
        ),
      ),
    );
  }
}


