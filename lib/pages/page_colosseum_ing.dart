import 'package:flutter/material.dart';
import 'package:pokemon_app/components/component_colosseum_in_monster_item.dart';
import 'package:pokemon_app/model/colosseum_item.dart';
import 'dart:math';

class PageColosseumIng extends StatefulWidget {
  const PageColosseumIng({
    super.key,
    required this.monster1,
    required this.monster2,
  });

  // 스피드가 같을시 랜덤불리언 사용을 위함
  get boolValue => Random().nextBool();
  // 생성자를 통해 몬스터 정보를 받아옴
  final ColosseumItem monster1;
  final ColosseumItem monster2;

  @override
  State<PageColosseumIng> createState() => _PageColosseumIngState();
}

class _PageColosseumIngState extends State<PageColosseumIng> {

  bool _isTurnMonster1 = true; // 기본으로 1번 몬스터 선제공격, 1번 순서면 2번 순서 아님
  bool _monster1Live = true; // 살아 있는지
  num _monster1CurrentHp = 0; // 현재 체력 변수 만들기
  bool _monster2Live = true;
  num _monster2CurrentHp = 0;

  String _gameLog = '';
  
  @override
  void initState() {
    super.initState();
    _calculateFirstTurn(); // 선제 공격 판별
    setState(() {
      _monster1CurrentHp = widget.monster1.hp; // 1번 몬스터 잔여 hp (첫 페이지니까 최대최력만큼)
      _monster2CurrentHp = widget.monster2.hp;
    });
  }
  
  // 선제 공격 계산
  void _calculateFirstTurn() {
    if (widget.monster1.speed < widget.monster2.speed) { // 2번 몬스터가 빠르면
      setState(() {
        _isTurnMonster1 = false;
      });
    } else if (widget.monster1.speed == widget.monster2.speed) {
      setState(() {
        _isTurnMonster1 = widget.boolValue;
      });
    }
  }

  // 최종 데미지 계산 (크리티컬 계산식 넣기)
  num _calculateResultHitPoint( num myHitPower, num targetDefPower) {
    List<num> criticalArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]; // 10% 확률로 크리티컬
    criticalArr.shuffle(); // 섞기

    bool isCritical = false; // 기본은 크리티컬 안터진다
    if ( criticalArr[0] < 6 ) isCritical = true; // 다 섞은 배열의 0번째 요소가 1이면 크리티컬로 생각한다

    num resultHit = myHitPower; // 공격력
    if (isCritical) resultHit = resultHit * 2; // 크리시 2배 데미지
    resultHit = resultHit - targetDefPower; // 방어력만큼 데미지 감소
    resultHit = resultHit.round(); // 반올림

    if(resultHit <= 0) resultHit = 1; // 데미지가 0이거나 음수면 1데미지로 처리

    return resultHit;
  }

  // 상대 몬스터가 죽었는지 확인
  void _checkIsDead(num targetMonster) {
    if (targetMonster == 1 && (_monster1CurrentHp <= 0)) { // 1번 몬스터의 체력이 0이거나 작으면 죽음
      setState(() {
        _monster1Live = false;
      });
    } else if (targetMonster == 2 && (_monster2CurrentHp <= 0)) {
      setState(() {
        _monster2Live = false;
      });
    }
  }

  // 공격처리
  void _attMonster(num actionMonster) {
    num myHisPower = widget.monster1.attack;
    num targetDefPower = widget.monster2.defense;

    if (actionMonster == 2) {
      myHisPower = widget.monster2.attack;
      targetDefPower = widget.monster1.defense;
    }

    num resultHit = _calculateResultHitPoint(myHisPower, targetDefPower); // 상대 Hp 몇 까야하는지 계산

    if (actionMonster == 1) { // 공격하는 몬스터가 1번이면 (공격당하는 몬스터가 2번)
      setState(() {
        _monster2CurrentHp -= resultHit; // 2번 몬스터 체력 깎기
        if (_monster2CurrentHp <= 0) _monster2CurrentHp = 0; // 체력 0 미만이면 0으로 고정
        _checkIsDead(2); // 2번 몬스터 죽었는지 확인
      });
    } else {
      setState(() {
        _monster1CurrentHp -= resultHit; // 1번 몬스터 체력 깎기
        if (_monster1CurrentHp <= 0) _monster1CurrentHp = 0; // 체력 0 미만이면 0으로 고정
        _checkIsDead(1); // 1번 몬스터 죽었는지 확인
      });
    }

    setState(() {
      _isTurnMonster1 = !_isTurnMonster1; // 턴 넘기기. not 연산자로 계속 바꿔주기
    });

  }

  @override
  Widget build(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width; // 반응형 쓰겠다

    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
            'assets/pokemon_logo.png',
          width: phoneWidth / 4,
          height: phoneWidth / 20,
        ),
        backgroundColor: Colors.cyanAccent,
        centerTitle: true,
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width; // 반응형 쓰겠다

    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.all(phoneWidth / 20), //여백 줌

        child: Column(
          children: [
            Row(
              children: [
                SizedBox(
                  width: phoneWidth / 2.5, // 화면의 1/2.5만큼 쓰겠다
                  child: ComponentColosseumInMonsterItem(
                    colosseumItem: widget.monster1,
                    callback: () {
                      _attMonster(1); // 탭할 시 1번 몬스터 공격
                    },
                    isMyTurn: _isTurnMonster1, // 턴인가
                    isLive: _monster1Live, // 살아 있는가
                    currentHp: _monster1CurrentHp, // 현재 체력
                  ),
                ),
                SizedBox(
                  width: phoneWidth / 10,
                ),
                SizedBox(
                  width: phoneWidth / 2.5,
                  child: ComponentColosseumInMonsterItem(
                    colosseumItem: widget.monster2,
                    callback: () {
                      _attMonster(2);
                    },
                    isMyTurn: !_isTurnMonster1,
                    isLive: _monster2Live,
                    currentHp: _monster2CurrentHp,
                  ),
                ),
              ],
            ),
            Text(_gameLog),
          ],
        ),
      ),
    );
  }
}
