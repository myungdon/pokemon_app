import 'package:dio/dio.dart';
import 'package:pokemon_app/config/config_api.dart';
import 'package:pokemon_app/model/pokemon_detail_result.dart';
import 'package:pokemon_app/model/pokemon_list_result.dart';

class RepoPokemon {
  Future<PokemonListResult> getPokemons() async {
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/pokemon/all';

    final response = await dio.get( // 기다려야 해, http 메서드는 방식은 get
      _baseUrl, // 어디에 전화할지 적어준다
      options: Options(
        followRedirects: false, // 다른데로 따라 갈건지
        validateStatus: (status) {
          return status == 200; //200번일 때만 성공
        }
      )
    );

    return PokemonListResult.fromJson(response.data);
  }

  Future<PokemonDetailResult> getPokemon(num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/pokemon/detail/{id}';

    final response = await dio.get(
      _baseUrl.replaceAll('{id}', id.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return PokemonDetailResult.fromJson(response.data);
  }
}