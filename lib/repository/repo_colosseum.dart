import 'package:dio/dio.dart';
import 'package:pokemon_app/config/config_api.dart';
import 'package:pokemon_app/model/colosseum_state_result.dart';
import 'package:pokemon_app/model/common_result.dart';

class RepoColosseum {
  Future<ColosseumStateResult> getCurrentState() async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/colosseum/colosseum-in-monster';

    final response = await dio.get(
        _baseUrl,
      options: Options(
        followRedirects: false,
        validateStatus: (state) {
          return state == 200;
        })
    );

    return ColosseumStateResult.fromJson(response.data);
  }

  Future<CommonResult> setColosseumIn(num monsterId) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/colosseum/in/{monsterId}';

    final response = await dio.post(
        _baseUrl.replaceAll('{monsterId}', 'monsterId'),
      options: Options(
        followRedirects: false,
        validateStatus: (state) {
          return state == 200;
        }
      )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> delColosseumOut(num colosseumId) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/colosseum/out/{colosseumId}';

    final response = await dio.delete(
        _baseUrl.replaceAll('{colosseumId}', 'colosseumId'),
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }
}